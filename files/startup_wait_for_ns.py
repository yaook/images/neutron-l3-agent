from pathlib import Path
import collections
import logging
import openstack
import os
import psutil
import re
import sys


logging_level = os.getenv('STARTUP_LOG_LEVEL', 'INFO').upper()
logging.basicConfig(
    level=logging_level,
    format='[%(asctime)s] StartupProbe %(levelname)s %(message)s'
)


def _get_os_client():
    return openstack.connect()


def _get_local_ns():
    ns_path = Path('/var/run/netns')
    return_set = set()
    logging.debug("Adding every local ns to a set")
    for ns in ns_path.glob('qrouter-*'):
        return_set.add(ns.name[len("qrouter-"):])
    return return_set


def _get_local_keepalived_processes():
    logging.debug("Generating List of all keepalived processes")
    router_processes = collections.Counter()
    for proc in psutil.process_iter():
        try:
            if proc.name() == "keepalived":
                # 4th argument is the path to the keepalived config. This path
                # contains the uuid of the router.

                cmdline_args = proc.cmdline()
                if len(cmdline_args) < 4:
                    continue

                path = cmdline_args[3]
                # using regex to extract uuid from path

                router_id_match = re.findall(
                        '[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-' +
                        '[a-fA-F0-9]{4}-[a-fA-F0-9]{12}',
                        path)
                if router_id_match:
                    router_id = router_id_match[0]
                    router_processes[router_id] += 1

        except (psutil.NoSuchProcess, psutil.AccessDenied,
                psutil.ZombieProcess):
            pass
    logging.debug("Creating a set containing all routers with two " +
                  "keepalived processes")
    return set(router_id
               for router_id, count
               in router_processes.items()
               if count == 2)


def _get_routers():
    logging.debug("Get openstack client")
    client = _get_os_client()
    host = os.getenv('HOSTNAME')
    if host is not None:
        logging.debug("Getting l3 agent")
        l3, = client.network.agents(binary="neutron-l3-agent",
                                    host=host)
        logging.debug("Adding every router to a set")
        ha_router_ids = set()
        router_ids = set()
        for router in client.network.agent_hosted_routers(l3.id):
            if router.is_admin_state_up:
                router_ids.add(router.id)
                if router.is_ha:
                    ha_router_ids.add(router.id)
        return router_ids, ha_router_ids
    else:
        raise KeyError("HOSTNAME needs to be set in the environment.")


def sync_complete():
    logging.debug("Get all routers")
    router_set, ha_router_set = _get_routers()
    logging.info(f"Found {len(router_set)} routers.")
    logging.info(f"Found {len(ha_router_set)} ha routers.")
    logging.debug("Get all local namespaces")
    local_ns_set = _get_local_ns()
    logging.info(f"Found {len(local_ns_set)} namespaces.")
    logging.debug("Get all local keepalived processes")
    local_proc_set = _get_local_keepalived_processes()
    logging.info(f"Found {len(local_proc_set)} routers with keepalived " +
                 "processes.")
    logging.debug("Comparing...")
    return router_set == local_ns_set and ha_router_set == local_proc_set


def main():
    logging.debug("Starting Sync..")
    if sync_complete():
        logging.debug("Sync completed. Return 0")
        return 0
    else:
        logging.debug("Sync not complete. Return 1")
        return 1


if __name__ == '__main__':
    sys.exit(main())
