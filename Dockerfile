##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

# we need this default so renovate works
ARG release=train

FROM registry.yaook.cloud/yaook/neutron-agent-${release}:1.0.55

ARG release

RUN set -eux; \
    yum install -y keepalived haproxy radvd ; \
    yum clean all

RUN (git clone https://github.com/openstack/requirements.git --depth 1 --branch stable/${release} || \
    git clone https://github.com/openstack/requirements.git --depth 1 --branch ${release}-eol) && \
    pip3 install -c requirements/upper-constraints.txt python-openstackclient

COPY files/*-runner.sh /
COPY files/startup_wait_for_ns.py /

RUN ln -s /usr/local/bin/neutron-keepalived-state-change /bin/neutron-keepalived-state-change
